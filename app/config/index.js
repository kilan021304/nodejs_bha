const { default: mongoose } = require("mongoose");

const connect = async () => {
  await mongoose
    .connect("mongodb://localhost:27017/nodejs_api")
    .then(() => {
      console.log("Database is connected successfully");
    }).catch((err) => console.log(err));
};

module.exports = { connect };
