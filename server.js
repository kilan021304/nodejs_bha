const express = require("express");
const cors = require("cors");
const env = require("dotenv");

const db = require("./app/models");

const app = express();
env.config();

const corsOptions = {
  origin: "http://localhost:8081",
};

app.use(express.json());
app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: true }));

app.get("/", (_, res) => {
  res.json({
    message: "Welcome to bezkoder application.",
  });
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

db.mongoose
  .connect(db.url, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch((err) => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });
